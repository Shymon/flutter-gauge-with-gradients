import 'package:flutter/material.dart';
import 'package:flutter_app2/gauge.dart';
import 'package:quiver/async.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _start = 8000;
  int _current = 0;

  void _startTimer() {
    CountdownTimer countDownTimer = new CountdownTimer(
      Duration(seconds: _start),
      Duration(milliseconds: 200),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {
      if (duration.elapsed.inMilliseconds * 2 > 9000) sub.cancel();
      setState(() {
        _current = duration.elapsed.inMilliseconds * 2;
      });
    });

    sub.onDone(() {
      print("Done");
      sub.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: Container(
        width: width,
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Container(width: width / 1, child: Gauge(value: _current.toDouble())),
                  // Container(width: width / 5, child: Gauge(value: _current.toDouble())),
                  // Container(width: width / 5, child: Gauge(value: _current.toDouble())),
                  // Container(width: width / 5, child: Gauge(value: _current.toDouble())),
                  // Container(width: width / 5, child: Gauge(value: _current.toDouble())),
                ],
              ),
            ),
            RaisedButton(
              onPressed: _startTimer,
              child: Text('start'),
            ),
          ],
        ),
      ),
    );
  }
}
