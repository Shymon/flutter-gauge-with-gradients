import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:math' as math;

class Gauge extends StatefulWidget {
  final double value;

  const Gauge({Key key, this.value}) : super(key: key);

  @override
  _GaugeState createState() => _GaugeState();
}

class _GaugeState extends State<Gauge> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  double get _value => widget.value;

  @override
  Widget build(BuildContext context) {
    double animationValue = _ValueToRadians().calc(_value);

    return Column(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: [
                CustomPaint(
                  painter: _IndicatorBackgroundPainter(animationValue),
                  child: Container(),
                ),
                RepaintBoundary(
                  child: CustomPaint(
                    painter: _StaticElementsPainter(),
                    child: Container(),
                  ),
                ),
                CustomPaint(
                  foregroundPainter: _IndicatorPainter(animationValue),
                  child: Container(),
                ),
                CustomPaint(
                  foregroundPainter: _TicksWithNumbersPainter(animationValue),
                  child: Container(),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _StaticElementsPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var radius = size.width / 2;

    var paint = Paint()
      ..color = Colors.orangeAccent
      ..strokeWidth = 3
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    var path = Path();
    path.addOval(Rect.fromCircle(
      center: Offset(size.width / 2, size.height / 2),
      radius: radius,
    ));
    canvas.drawPath(path, paint);

    paint = Paint()
      ..color = Colors.grey
      ..strokeWidth = radius / 18
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    path = Path();
    path.addOval(Rect.fromCircle(
      center: Offset(size.width / 2, size.height / 2),
      radius: radius / 8,
    ));
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class _ValueToRadians {
  double rangeStart = 0;
  double rangeEnd = 320000;

  // Ranges start other than 0 is not implemented
  double calc(double value) {
    return (value * (3 / 4) * 2 * math.pi) / rangeEnd;
  }
}

class _IndicatorBackgroundPainter extends CustomPainter {
  final double radians;

  _IndicatorBackgroundPainter(this.radians);

  @override
  void paint(Canvas canvas, Size size) {
    var localRadians = radians;

    if (localRadians > (3 / 4) * 2 * math.pi) {
      localRadians = 2 * (3 / 4) * 2 * math.pi - localRadians;
    }

    localRadians -= (1 / 4) * math.pi;
    localRadians += math.pi;
    Offset center = Offset(size.width / 2, size.height / 2);

    var backgroundGradientPaint;

    backgroundGradientPaint = Paint()
      ..shader = RadialGradient(
        colors: [
          Colors.red,
          Colors.transparent,
        ],
      ).createShader(Rect.fromCircle(
        center: Offset(size.width / 2, size.height / 2),
        radius: size.width / 1,
      ));
    canvas.drawArc(
      Rect.fromCenter(
        center: center,
        height: size.width / 1,
        width: size.width / 1,
      ),
      -(1 / 4) * math.pi + math.pi - 0.03,
      localRadians - (3 / 4) * math.pi + 0.03,
      true,
      backgroundGradientPaint,
    );

    backgroundGradientPaint = Paint()
      ..shader = SweepGradient(
              startAngle: -(1 / 4) * math.pi + math.pi - 0.03,
              endAngle: -(1 / 4) * math.pi + math.pi - 0.03 + (3 / 4) * 3 * math.pi,
              colors: [
                Colors.grey[900],
                Colors.transparent,
                Colors.red,
              ],
              stops: const <double>[0.05, 0.35, 1],
              transform: GradientRotation(math.pi / 4))
          .createShader(Rect.fromCircle(
        center: Offset(size.width / 2, size.height / 2),
        radius: size.width / 4,
      ));
    canvas.drawArc(
      Rect.fromCenter(
        center: center,
        height: size.width / 1,
        width: size.width / 1,
      ),
      -(1 / 4) * math.pi + math.pi - 0.03,
      localRadians - (3 / 4) * math.pi + 0.03,
      true,
      backgroundGradientPaint,
    );
  }

  @override
  bool shouldRepaint(_IndicatorBackgroundPainter oldDelegate) {
    return (oldDelegate.radians - radians).abs() > 0.3;
  }
}

class _IndicatorPainter extends CustomPainter {
  final double radians;

  _IndicatorPainter(this.radians);

  @override
  void paint(Canvas canvas, Size size) {
    var localRadians = radians;

    if (localRadians > (3 / 4) * 2 * math.pi) {
      localRadians = 2 * (3 / 4) * 2 * math.pi - localRadians;
    }

    localRadians -= (1 / 4) * math.pi;
    localRadians += math.pi;
    Offset center = Offset(size.width / 2, size.height / 2);

    final indicatorLength = size.width / 4;
    final indicatorGradientPaint = Paint()
      ..shader = RadialGradient(
        colors: [
          Colors.orangeAccent,
          Colors.deepOrange,
        ],
      ).createShader(Rect.fromCircle(
        center: Offset(size.width / 2, size.height / 2),
        radius: 50,
      ));

    var indicatorCircleRadius = size.width / 30;
    var path = Path();
    path.addOval(Rect.fromCircle(
      center: Offset(size.width / 2, size.height / 2),
      radius: indicatorCircleRadius,
    ));
    canvas.drawPath(path, indicatorGradientPaint);

    path = Path();

    Offset pointOnCircle = Offset(
      indicatorLength * math.cos(localRadians) + center.dx,
      indicatorLength * math.sin(localRadians) + center.dy,
    );

    var innerRadius = indicatorCircleRadius;
    Offset pointA = Offset(
      -innerRadius * math.cos(localRadians + math.pi / 2) + center.dx,
      -innerRadius * math.sin(localRadians + math.pi / 2) + center.dy,
    );

    Offset pointB = Offset(
      innerRadius * math.cos(localRadians + math.pi / 2) + center.dx,
      innerRadius * math.sin(localRadians + math.pi / 2) + center.dy,
    );

    path.moveTo(pointA.dx, pointA.dy);
    path.lineTo(pointB.dx, pointB.dy);
    path.lineTo(pointOnCircle.dx, pointOnCircle.dy);
    path.close();
    canvas.drawPath(path, indicatorGradientPaint);
  }

  @override
  bool shouldRepaint(_IndicatorPainter oldDelegate) {
    return (oldDelegate.radians - radians).abs() > 0.3;
  }
}

class _TicksWithNumbersPainter extends CustomPainter {
  final double radians;

  _TicksWithNumbersPainter(this.radians);

  @override
  void paint(Canvas canvas, Size size) {
    var localRadians = radians;

    if (localRadians > (3 / 4) * 2 * math.pi) {
      localRadians = 2 * (3 / 4) * 2 * math.pi - localRadians;
    }

    localRadians -= (1 / 4) * math.pi;
    localRadians += math.pi;

    var path;
    Offset center = Offset(size.width / 2, size.height / 2);

    final textStyle = TextStyle(color: Colors.white54, fontSize: size.width / 15, fontWeight: FontWeight.bold);
    final activeTextStyle = TextStyle(color: Colors.deepOrange, fontSize: size.width / 15, fontWeight: FontWeight.bold);

    var markPaint = Paint()
      ..color = Colors.white54
      ..strokeWidth = size.width / 65
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    var activeMarkPaint = Paint()
      ..color = Colors.deepOrange
      ..strokeWidth = size.width / 65
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    var marksCount = 17;
    var radiusStart = size.width / 2.7;
    var markLength = size.width / 15;
    var startAngle = math.pi - (1 / 4) * math.pi;
    var maxAngle = (3 / 4) * 2 * math.pi;

    var radiusEnd = radiusStart + markLength;
    // Big marks
    for (int i = 0; i < marksCount; i++) {
      var currentAngle = startAngle + (i / (marksCount - 1)) * maxAngle;
      var firstAngle = math.cos(currentAngle);
      var secondAngle = math.sin(currentAngle);

      path = Path();
      var startPoint = Offset(firstAngle * radiusStart + center.dx, secondAngle * radiusStart + center.dy);
      path.moveTo(startPoint.dx, startPoint.dy);
      var endPoint = Offset(firstAngle * radiusEnd + center.dx, secondAngle * radiusEnd + center.dy);
      path.lineTo(endPoint.dx, endPoint.dy);
      path.close();
      canvas.drawPath(path, currentAngle > localRadians ? markPaint : activeMarkPaint);

      var textRadius = radiusStart - size.width / 13;
      var textPoint = Offset(firstAngle * textRadius + center.dx, secondAngle * textRadius + center.dy);

      final textSpan = TextSpan(
        text: (i * 20).toString(),
        style: currentAngle > localRadians ? textStyle : activeTextStyle,
      );
      final textPainter = TextPainter(
        text: textSpan,
        textDirection: TextDirection.ltr,
      );
      textPainter.layout(
        minWidth: 0,
        maxWidth: 100,
      );
      textPainter.paint(canvas, textPoint + Offset(-size.width / 70, -size.width / 20));
    }

    // Mini marks
    radiusStart = size.width / 2.5;
    markLength = size.width / 14;
    markPaint.strokeWidth = size.width / 120;
    activeMarkPaint.strokeWidth = size.width / 120;
    startAngle += maxAngle / (marksCount - 1) / 2;
    for (int i = 0; i < marksCount - 1; i++) {
      var currentAngle = startAngle + (i / (marksCount - 1)) * maxAngle;

      var firstAngle = math.cos(currentAngle);
      var secondAngle = math.sin(currentAngle);

      path = Path();
      var startPoint = Offset(firstAngle * radiusStart + center.dx, secondAngle * radiusStart + center.dy);
      path.moveTo(startPoint.dx, startPoint.dy);
      var endPoint = Offset(firstAngle * radiusEnd + center.dx, secondAngle * radiusEnd + center.dy);
      path.lineTo(endPoint.dx, endPoint.dy);
      path.close();
      canvas.drawPath(path, currentAngle > localRadians ? markPaint : activeMarkPaint);
    }

    startAngle += maxAngle / (marksCount - 1) / 4;
    for (int i = 0; i < marksCount - 1; i++) {
      var currentAngle = startAngle + (i / (marksCount - 1)) * maxAngle;

      var firstAngle = math.cos(currentAngle);
      var secondAngle = math.sin(currentAngle);

      path = Path();
      var startPoint = Offset(firstAngle * (radiusStart + 5) + center.dx, secondAngle * (radiusStart + 5) + center.dy);
      path.moveTo(startPoint.dx, startPoint.dy);
      var endPoint = Offset(firstAngle * radiusEnd + center.dx, secondAngle * radiusEnd + center.dy);
      path.lineTo(endPoint.dx, endPoint.dy);
      path.close();
      canvas.drawPath(path, currentAngle > localRadians ? markPaint : activeMarkPaint);
    }

    startAngle -= 9 / (marksCount - 1) / 4;
    for (int i = 0; i < marksCount - 1; i++) {
      var currentAngle = startAngle + (i / (marksCount - 1)) * maxAngle;

      var firstAngle = math.cos(currentAngle);
      var secondAngle = math.sin(currentAngle);

      path = Path();
      var startPoint = Offset(firstAngle * (radiusStart + 5) + center.dx, secondAngle * (radiusStart + 5) + center.dy);
      path.moveTo(startPoint.dx, startPoint.dy);
      var endPoint = Offset(firstAngle * radiusEnd + center.dx, secondAngle * radiusEnd + center.dy);
      path.lineTo(endPoint.dx, endPoint.dy);
      path.close();
      canvas.drawPath(path, currentAngle > localRadians ? markPaint : activeMarkPaint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
